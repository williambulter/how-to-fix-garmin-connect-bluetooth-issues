Garmin connect is very easy to connect and use, it has Bluetooth feature for wireless connectivity. You can wear the device and track your overall physical activity anywhere. 

But sometimes, it may encounter some problems and you may experience connection issues. These technical glitches can be easily fixed without any trouble. All you need to do is understand its functionality and the possible reasons behind the issue. 

Let�s find out the best ways to troubleshoot Bluetooth issues on your Garmin Connect. Follow the procedural guide in order to fix the issue:


# Android and iOS: #

## You may experience following symptoms: ##

* Garmin device connecting initially but then disconnects for a longer time interval.
* Garmin device not displaying smart Notifications.
* Device widgets are not working (Calendar, Weather, etc.)

To fix the issue, first look for the device button. You'll find this at the top of My Day view option in your Connect mobile. The device button will show the preferred activity status.

## Status Indicators: ##

A green indicating dot on your device shows that it is currently connected and ready to sync.

A red indicating dot displays that there is a connectivity issue with your device. There can be following issues:

* Bluetooth is not enabled on your phone.
* Garmin connected is currently under maintenance or facing any technical issue.

### Note: ###
No dot on your device indicates that device is currently paired but it is not connected. Follow the steps:

1. Keep the Garmin device in Bluetooth range of your phone.
2. Restart both Garmin device and phone. Doing this will possibly solve all your issues. Reboot your device for at least 1 minute, and then, open and connect both of them. 
Proceed to next step as this won't solve the issue.
3. Remove and add the Garmin device back to your mobile. Click here to know how to remove and connect devices back.

## If you continue to face the connected issues: ##

* Check if there are any updates available for your connect mobile application. Go to the app store on your phone and install the latest version updates.
* Check and confirm that your device is running the latest software version. Connect it to a PC and install the Garmin Express.
* Garmin Express will check and install the available updates automatically.
* Check and verify that your phone is updated to its latest version. 
* Check for the Minimum Bluetooth Requirements to make sure that it meets device's system requirements.


# Windows 10 Mobile: #

## Follow the steps to resolve the error: ##

1.Verify that Bluetooth is enabled on your phone-

* Swipe down from the top of your phone screen to access Quick action tools.
* Check and confirm that Bluetooth is enabled or turned on.

2.Determine connection status of Garmin device with connect mobile-

* Launch Connect mobile application
* Tap on the Menu option
* In the right-side corner, tap on the Garmin device icon.
* Check for the device status, and then, follow the below mentioned steps according to the device status:

## Garmin device is indicated as ''Not Connected'': ##

1. Restart both your phone and Garmin device. Rebooting them generally solve all the connection issues. Let it reboot for about 1 minute.
2. Open the Connect mobile app and go to the device status/settings view.
3. Give them 1-2 minutes to reconnect back and display status as ''Connected''.

## Phone and Garmin device fail to connect back after 1-2 minutes after performing a restart: ##

1. Tap on three dots at the lower right-hand side of the screen.
2. Tap on 'Remove Device'
3. Tap on Yes to confirm and unpair the device.
4. Add the device again to the Connect Mobile.
5. After pairing, Garmin device should be connected to the mobile app.

## Connected Mobile app indicating as ''No Devices Paired'': ##

1. Tap on Add Device.
2. Select the model that you're trying to connect.
3. Follow the pairing prompts.
4. After pairing, Garmin device should be connected to the mobile app.

## Connected Mobile app displaying ''Bluetooth is turned off'': ##

1. Tap on Turn it on.
2. Tap on Turn on Bluetooth.
3. Give the device about 1-2 minutes to show the status as ''Connected''.


## If you are still having the connectivity issues: ##

* Check if there are any updates available for your connect mobile application. Go to the app store on your phone and install the latest version updates.
* Check and confirm that your device is running the latest software version. Connect it to a PC and install Garmin Express.
* Garmin Express will check and install the available updates automatically.
* Check and verify that your phone is updated to its latest version. 
* Check for the Minimum Bluetooth Compatibility Requirements to make sure that it meets the device's system requirements.


If you are still experiencing Bluetooth connectivity issues or any other technical error with your Garmin device, then you can contact Garmin customer service phone numbar to troubleshoot the issue within minutes.

### Visit Us: 800support.net/garmin-support ###